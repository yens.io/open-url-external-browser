//GLOBAL VARIABLES
var chromeWindow;
var isFirefox;
var isChrome;
var isIE;
var isEdge;
var bits;

//this code is executed when page is done loading
$(document).ready(function () {
    detectBrowser();
    clickHandlers();
    detectOSBits();
    if (isIE || isEdge) {
        chromeWindow = new ActiveXObject("WScript.shell");
    }
});

//rebinds the click actions
var clickHandlers = function () {
    $('#chrome-link').on('click', function (e) {
        //prevents the click from instantly redirecting to the URL
        e.preventDefault();
        //get the url out of the clicked link
        var url = $(this).attr('href');
        //checks if OS is running 64-bit
        if (bits != 64) {
            alert('only 64 bit OS is supported');
            //stops rest of function if it's not running 64-bit
            return;
        }
        if (isChrome) {
            console.log('chrome detected');
            window.location.href = url;
        }
        if (isFirefox) {
            console.log('firefox detected');
            var c = confirm('The redirect only works in Internet Explorer, do you want to continue in Firefox?');
            if (c) { window.location.href = url; }
        }
        if (isIE || isEdge) {
            console.log('IE detected');
            try {
                chromeWindow.run('"c:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe" ' + url);
            }
            catch(error){
                //error handling
                console.log(error);
                alert('there was an error trying to execute the shell command, you probably don\'t have Google Chrome installed or it\'s not installed in Program Files(x86)/Google/Chrome/Application');}
        }
        else {
            //some more error handling
            alert('Browser is not supported');
        }
    });
}

var detectBrowser = function () {
    // Firefox 1.0+
    isFirefox = typeof InstallTrigger !== 'undefined';
    // Internet Explorer 6-11
    isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+ 
    isEdge = !isIE && !!window.StyleMedia;
    // Chrome 1+
    isChrome = !!window.chrome && !!window.chrome.webstore;
}

//detects if windows is running in 32 or 64 bits
var detectOSBits = function () {
    if (navigator.userAgent.indexOf("WOW64") != -1 ||
        navigator.userAgent.indexOf("Win64") != -1) {
        bits = 64;
    } else {
        bits = 32;
    }
}

